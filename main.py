#!/usr/bin/python2
# -*- coding: utf8 -*-
import sys, os, requests, json, logging
#import sqlite3
from time import sleep
from lxml import html


#/----------------------------------------------------------------------------/

def CB_ayuda(variables):
  salida = """¡Hola, mi función es mostrar sinopsis de películas, series y libros!
Por ahora respondo a estos comandos:
- /ayuda -> muestra este mismo texto.
- /peli nombre de la peli -> Muestra la sinopsis de la película y la fecha de estreno.
- /serie nombre de la serie -> Muestra la sinopsis de la serie y la fecha de estreno.
- /libro nombre del libro -> Muestra la sinopsis del libro, el título original y el autor.
Mi código es libre y puede ser consultado en GitHub:
https://github.com/helfio/frikibot
"""
            
  return salida


def CB_serie(variables):
  direccion = "http://www.omdbapi.com/?"
  titulo = ''
  for palabra in variables:
    titulo = titulo+' '+palabra
  titulo = titulo.lstrip()
  datos = {'t':titulo,'plot':'full','type':'series'}
  respuesta = requests.get(direccion,params=datos)
  if str(respuesta.status_code) == "200":
    paquete = respuesta.json()
    if paquete['Response'] == "True":
      #salida = "Titulo: *" + paquete['Title'].encode('utf-8') + "*\n"
      salida = "Titulo: " + paquete['Title'].encode('utf-8') + "\n"
      #salida = salida + "Genero: _" + paquete['Genre'].encode('utf-8') + "_\n"
      salida = salida + "Genero: " + paquete['Genre'].encode('utf-8') + "\n"
      salida = salida + "Fecha: " + paquete['Released'].encode('utf-8') +'\n'
      salida = salida + '\nSinopsis:\n'+paquete['Plot'].encode('utf-8')
      #print salida.decode('utf-8')
      return salida.decode('utf-8')
    else:
      return "No encuentro ese título :("
  return "Ha habido un error :S"


def CB_peli(variables):
  direccion = "http://www.omdbapi.com/?"
  titulo = ''
  for palabra in variables:
    titulo = titulo+' '+palabra
  titulo = titulo.lstrip()
  datos = {'t':titulo,'plot':'full','type':'movie'}
  respuesta = requests.get(direccion,params=datos)
  if str(respuesta.status_code) == "200":
    paquete = respuesta.json()
    if paquete['Response'] == "True":
      #salida = "Titulo: *" + paquete['Title'].encode('utf-8') + "*\n"
      salida = "Titulo: " + paquete['Title'].encode('utf-8') + "\n"
      salida = salida + "Genero: " + paquete['Genre'].encode('utf-8') + "\n"
      salida = salida + "Fecha: " + paquete['Released'].encode('utf-8') +'\n'
      salida = salida + '\nSinopsis:\n'+paquete['Plot'].encode('utf-8')
      return salida.decode('utf-8')
    else:
      return "No encuentro ese título :("
  return "Ha habido un error :S"


def CB_prueba(variables):
  direccion = "http://www.filmaffinity.com"
  titulo = ''
  for palabra in variables:
    if '/' not in palabra:
      titulo = titulo+'+'+palabra
  titulo = titulo.lstrip('+')
  datos = {'stext':titulo,'stype[]':'title','country':'','genre':'','fromyear':'','toyear':''}
  respuesta = requests.get(direccion+'/es/advsearch.php', params=datos)
  if str(respuesta.status_code) == "200":
    # Aquí habría que añadir comprobación para ver si ha encontrado el libro o no.
    arbol = html.fromstring(respuesta.text)
    if 'No' not in arbol.xpath("//div[@id='adv-search-no-results']/b/text()"):
      #try:
        direccion2 = direccion + arbol.xpath("//div[@class='mc-title']/a/@href")[0]
        respuesta2 = requests.get(direccion2)
        arbol2 = html.fromstring(respuesta2.text)
        sinopsis = arbol2.xpath("//dl[@class='movie-info']/dd/text()")[-1]
        fecha = arbol2.xpath("//dl[@class='movie-info']/dd/text()")[1]
        titulo = arbol2.xpath("//meta[@property='og:title']/@content")[0]
        mas_info = arbol2.xpath("//meta[@property='og:url']/@content")[0]
        #salida = 'Título: '+titulo.encode('utf-8')+'\n'
        #salida = salida + 'Páginas: '+fecha.encode('utf-8')+'\n'
        #salida = salida + '\nSinopsis:\n'+sinopsis.encode('utf-8')+'\n'
        #salida = salida + '\nMás info:\n'+mas_info
        #return salida.decode('utf-8')
        #salida = u'Título: *'+titulo+'*\n'
        salida = u'Título: '+titulo+'\n'
        salida = salida + u'Año: '+fecha+'\n'
        salida = salida + u'\nSinopsis:\n'+sinopsis+'\n'
        salida = salida + u'\nMás info:\n'+mas_info
        print salida
        return salida
      #except:
        #return 'No encuentro ese título :(('
    else:
      return 'No encuentro ese título :('


def CB_libro(variables):
  direccion = "http://www.todostuslibros.com/busquedas/?titulo="
  titulo = ''
  for palabra in variables:
    if '/' not in palabra:
      titulo = titulo+'+'+palabra
  titulo = titulo.lstrip('+')
  respuesta = requests.get(direccion+titulo)
  if str(respuesta.status_code) == "200":
    #paquete = respuesta.json()
    # Aquí habría que añadir comprobación para ver si ha encontrado el libro o no.
    arbol = html.fromstring(respuesta.text)
    if 'No hay' not in arbol.xpath('//*[@id="container"]/div/div/div[1]/span/text()')[0]:
      try:
        direccion2 = "http://www.todostuslibros.com"+arbol.xpath("//div[@class='image']/a/@href")[0]
        respuesta2 = requests.get(direccion2)
        arbol2 = html.fromstring(respuesta2.text)
        sinopsis = arbol2.xpath("//p[@itemprop='description']/text()")[0]
        pags = arbol2.xpath("//dd[@itemprop='numberOfPages']/text()")[0]
        #sinopsis = arbol.xpath('//*[@id="container"]/div/div/ul/li[1]/div[1]/div[2]/p/text()')[0]
        titulo_libro = arbol.xpath('//*[@id="container"]/div/div/ul/li[1]/div[1]/div[2]/h2/a/text()')[0]
        autor = arbol.xpath('//*[@id="container"]/div/div/ul/li[1]/div[1]/div[2]/h3/a/text()')[0]
        mas_info = arbol.xpath('//*[@id="container"]/div/div/ul/li[1]/div[1]/div[2]/h2/a/@href')[0]
        #salida = 'Título: *'+titulo_libro.encode('utf-8')+'*\n'
        salida = 'Título: '+titulo_libro.encode('utf-8')+'\n'
        #salida = salida + 'Autor: _'+autor.encode('utf-8')+'_\n'
        salida = salida + 'Autor: '+autor.encode('utf-8')+'\n'
        salida = salida + 'Páginas: '+pags.encode('utf-8')+'\n'
        salida = salida + '\nSinopsis:\n'+sinopsis.encode('utf-8')+'\n'
        salida = salida + '\nMás info:\n'+"http://www.todostuslibros.com"+mas_info
        #print sinopsis
        return salida.decode('utf-8')
      except:
        return 'No encuentro ese título :(('
    else:
      return 'No encuentro ese título :('
    
#/----------------------------------------------------------------------------/

def Nuevos_Msgs(umcon_id):
  if umcon_id == 0:
    r = requests.post(URL+"getUpdates")
    #logging.warn('Func. Nuevos mensajes. umcon_id = '+ str(umcon_id))
  else:
    #logging.warn('Func. Nuevos mensajes. umcon_id = '+ str(umcon_id))
    opciones = {'offset':umcon_id}
    sc = 0
    while sc != 200:
      try: 
        r = requests.post(URL+"getUpdates", params=opciones)
        sc = r.status_code
        if sc != 200:
          sleep(5)
          logging.warn('Error codigo: '+ str(r.status_code))
      except requests.exceptions.ConnectTimeout, e:
        logging.warn('Error en la petición. Timeout')
        sleep(5)
      except requests.exceptions.SSLError, e:
        logging.warn('Error en la petición. SSLError')
        sleep(5)
      except requests.exceptions.ConnectionError, e:
        logging.warn('Error en la petición. ConnectionError')
        sleep(5)
      except:
        logging.warn('Error desconocido.')
        sleep(10)
  return json.loads(r.text) # Aquí da un fallo algunas veces

def Iniciar(URL):
  inicio = requests.post(URL+"getME")
  return json.loads(inicio.text)['ok']

def CheckConfFileExistence(config_dir):
  import ConfigParser
  global config
  if os.path.exists(config_dir):
    config = ConfigParser.RawConfigParser()
    config.read('./config.cfg')
    return 1
  elif not os.path.exists(config_dir):
    import shutil
    shutil.copy("./config.cfg.new",config_dir)
    sys.exit()
  
def Analizar(msg_recibido, lista_comandos):
  variables = msg_recibido.split(' ')
  for s in lista_comandos:
    if s in variables:
      print "#### Se ha detectado un comando."
      logging.warn('Contenido:\n%s', msg_recibido)
      for i in range(variables.index(s)+1):
        del variables[0]
      return lista_comandos[s](variables)
  return "nada"

#/----------------------------------------------------------------------------/

if CheckConfFileExistence('./config.cfg'):
  TOKEN = config.get('BOT', 'TOKEN')
URL = "https://api.telegram.org/bot"+TOKEN+"/"
umcon_id = 0
comandos = {'/ayuda': CB_ayuda,'/start': CB_ayuda,'/libro': CB_libro,'/peli': CB_peli,'/prueba': CB_prueba,'/serie': CB_serie}

#/----------------------------------------------------------------------------/

LOG_FILENAME = 'logging.log'
global logging
logging.basicConfig(filename=LOG_FILENAME,level=logging.WARNING,format='%(asctime)s %(message)s')

if Iniciar(URL) == True:
  logging.warn('############ INICIANDO BOT ############')
  while True:
    datos = Nuevos_Msgs(umcon_id+1)
    j = 0
    if datos['result'] != []:
      nuevo_id = datos['result'][-1]['update_id']
      while umcon_id < nuevo_id:
        if 'text' in datos['result'][j]['message']:
          TEXTO =  datos['result'][j]['message']['text']
          CHAT_ID = datos['result'][j]['message']['chat']['id']
          logging.warn('Nuevo mensaje.')
          TEXTO = Analizar(TEXTO, comandos)
          if TEXTO != "nada":
            parametros = {'chat_id':CHAT_ID, 'text':TEXTO, 'disable_web_page_preview':1} #, 'parse_mode':'markdown'}
            r = requests.post(URL+'SendMessage', params=parametros)
            print r.status_code
            logging.warn('Respuesta enviada:\n%s', TEXTO)
          else:
            logging.warn('No se envía respuesta.')
            pass
        else:
          pass
        umcon_id = datos['result'][j]['update_id']
        j += 1
    sleep(1)
  
else:
  logging.warn('Error al iniciar el bot.')
  print "### Ha habido algún error al iniciar el bot!"
  pass